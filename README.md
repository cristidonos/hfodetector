HFOdetector

UNSUPERVISED DETECTION OF HIGH-FREQUENCY OSCILLATIONS USING TIME-FREQUENCY MAPS AND COMPUTER VISION

This repository provides a MATLAB GUI for the unsupervised HFO detector submitted for publication in Plos One. It contains two simulation datasets in mat files compatible with our GUI and RippleLab format.

For any questions/comments regarding the algorithms, as well as source code requests, please contact Cristi at cristian.donos@g.unibuc.ro